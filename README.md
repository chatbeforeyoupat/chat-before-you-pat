Chat Before You Pat is an innovative initiative created by Charlotte Bryan designed to promote awareness of the need for people (especially young children) to ask a dog's handler before they approaching their dog.

Founded in Australia in 2019, it is designed to promote dog safety and has an accompanying rhyme for children to learn:

Chat Before You Pat,
Talk To the Owner,
Don't Stare Into Their Eyes,
Pat Under Not Over.

Website : https://www.chatbeforeyoupat.com.au/